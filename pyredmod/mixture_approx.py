"""
mixture_approx.py

Author: James Ashton Nichols
Start date: December 2018
"""

import numpy as np

import warnings

_RESID_TOL = 1e-6
_EM_TOL = 1.0 

class MixtureEstimator(object):
    """
    A mixture of various estimators. Requires a marginal distribution
    for each estimator, otherwise won't work.
    """
    def __init__(self, estimators, probs, Wm=None):
        
        if len(estimators) != len(probs):
            raise ValueError(f'{self.__class__.__name__}: each estimator needs '
                              'exactly one probability')

        if estimators is not None:
            self._estimators = estimators
        
        self._probs = probs
         
        self._Wm = None
        self.Wm = Wm

    @property
    def Wm(self):
        return self._Wm
    @Wm.setter
    def Wm(self, new_Wm):
        if new_Wm is not None:
            if not new_Wm.is_orthonormal:
                raise Exception( f'{self.__class__.__name__}: Wm must be an orthonormal ' 
                                  'basis for correct reconstruction')
            if self._Wm is None or not new_Wm == self._Wm:
                if self._Wm is not None:
                    warnings.warn(f'{self.__class__.__name__}: Setting new Wm space')
    
                self._Wm = new_Wm 
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    for est in self.estimators:
                        est.Wm = new_Wm
    
    @property
    def estimators(self):
        return self._estimators

    @estimators.setter
    def estimators(self, new_est):
        self._estimators = new_est
    
    def __getitem__(self, idx):
        return (self._estimators[idx], self._probs[idx])

    def __setitem__(self, idx, estimator_and_prob):
        self._estimators[idx] = estimator_and_prob[0]
        self._probs[idx] = estimator_and_prob[1]
        if self.Wm:
            self._estimators[idx].Wm = self.Wm

    def __delitem__(self, idx):
        del self._estimators[idx]
        del self._probs[idx]

    def append(self, estimator_and_prob):
        if self.Wm:
            estimator_and_prob[0].Wm = self.Wm
        self._estimators.append(estimator_and_prob[0])
        self._probs.append(estimator_and_prob[1])

    def __iter__(self):
        return zip(self._estimators, self._probs)

    def truncate(self, idx):
        return type(self)([est.truncate(idx) for est in self.estimators], self._probs, Wm=self.Wm)

    def marginal(self, w_coords, Wm=None):
        self.Wm = Wm

        marginal = 0
        for estimator, prob in self:
            marginal += prob * estimator.marginal(w_coords)

        return marginal
    
    def log_density(self, u):
        return np.array([np.log(prob) + est.log_density(u) for est, prob in self])

    def sum_log_exp(self, u):
        est_log_density = self.log_density(u)
        est_log_density_max = est_log_density.max(axis=0)
        est_log_density = np.log(np.exp(est_log_density - est_log_density_max).sum(axis=0)) + est_log_density_max
        
        return np.exp(est_log_density - est_log_density.max()).sum(), est_log_density.max()

    def density_matrix(self, u, resid_tol=None):
        # This is a matrix that is used in the EM algorithm for estimating
        # better mixture algorithms.
        if resid_tol is None:
            resid_tol = _RESID_TOL
        
        est_log_density = self.log_density(u)
        est_density = np.exp(est_log_density - est_log_density.max(axis=0))
        
        in_span = np.array([est.in_span(u) for est, prob in self])
        
        # Set all densities that are out of span to 0
        est_density[~in_span] = 0.0
        with np.errstate(divide='ignore', invalid='ignore'):
            # For now we ignore points that are in the span of nothing
            rik = np.nan_to_num(est_density / est_density.sum(axis=0))
        return rik

    def EM_step(self, u):
        # Performs the E step
        r = self.density_matrix(u)
        # ...and the M step
        for i, est in enumerate(self.estimators):
            if r[i].sum() > _EM_TOL:
                est.update_from_samples(u, r[i])
                self._probs[i] = r[i].sum() / r.shape[1]
            else:
                del self[i]

        return r

    def best_estimate(self, w_coords, Wm=None):
        self.Wm = Wm
        
        est_log_margs = np.array([np.log(prob) + est.log_marginal(w_coords) for est, prob in self])
        est_margs = np.exp(est_log_margs - est_log_margs.max(axis=0))
        
        resids = np.array([est.Wm_residual(w_coords) for est, prob in self])
        in_span = (resids < _RESID_TOL)
        closest = np.argmin(resids, axis=0)

        out_all_span = ~np.any(in_span, axis=0)
        #if hasattr(out_all_span, 'ndim'): 
        # Weirdly np._bool has ndim...
        if out_all_span.ndim > 0:    
            for i, out in enumerate(out_all_span):
                if out:
                    in_span[closest[i], i] = True
        elif out_all_span:
            in_span[closest] = True

        # oooh here we *could* get some tensor magic. 
        # In fact... gauss_approx could be of type tensor!
        best_estimate = None
        for i, (est, prob) in enumerate(self):
            # This will be a Basis if there are multiple coords..
            if best_estimate is None:
                best_estimate = est.best_estimate(w_coords) * (in_span[i] * est_margs[i])
            else:
                best_estimate += est.best_estimate(w_coords) * (in_span[i] * est_margs[i])
            
        return best_estimate / est_margs.sum(axis=0)
            

