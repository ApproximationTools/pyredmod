"""
pw_utils.py

Author: James Ashton Nichols
Start date: June 2017

Some utils including a first implementation of the FEM scheme
"""

import math
import numpy as np
import scipy.linalg
import scipy.sparse
from itertools import *
import copy
import warnings

import pyhilbert as hil

__all__ = ['make_sin_basis','make_local_avg_random_basis','make_local_avg_grid_basis']


"""
*****************************************************************************************
All the functions below are for building specific basis systems, reduced basis, sinusoid, 
coarse grid hat functions, etc...
*****************************************************************************************
"""

def make_sin_basis(div, N=None):

    H1 = hil.H1DyadicSq(div)
    if N is None:
        N = H1.side_len 
    values = np.zeros((H1.n, N**2))
    count = 0

    x, y = np.meshgrid(H1.x_grid, H1.y_grid)
    # We want an ordering such that we get (1,1), (1,2), (2,1), (2,2), (2,3), ...
    for n in range(1,N+1):
        for m in range(1,n+1):
            def f(x,y): 
                return np.sin(n * math.pi * x) * np.sin(m * math.pi * y) * 2.0 / math.sqrt(math.pi * math.pi * (m * m + n * n))
            
            values[:, count] = f(x,y)[1:-1,1:-1].flatten()
            count += 1
            
            # We do the mirrored map here
            if m < n:
                def f(x,y): 
                    return np.sin(m * math.pi * x) * np.sin(n * math.pi * y) * 2.0 / math.sqrt(math.pi * math.pi * (m * m + n * n))

                values[:, count] = f(x,y)[1:-1,1:-1].flatten()
                count += 1

    return hil.Basis(values, space=H1)

def make_local_avg_random_basis(m, div, width=2, bounds=None, bound_prop=1.0, return_map=False):

    H1 = hil.H1DyadicSq(div)
    L2 = hil.L2DyadicSq(div)
    h = 2**(-div)

    full_points =  list(product(range(L2.side_len - width + 1), range(L2.side_len - width + 1)))
    
    if bounds is not None:
        bounds = np.array(np.round(bounds * L2.side_len), dtype=np.int)
        bound_points = list(product(range(bounds[0,0], bounds[0,1] - (width-1)), range(bounds[1,0], bounds[1,1] - (width-1))))
        remain_points = [p for p in full_points if p not in bound_points]
        remain_locs = np.random.choice(range(len(remain_points)), round(m * (1.0 - bound_prop)), replace=False)
    else:
        bound_points = full_points 
        remain_points = [] 
        remain_locs = []
        
    bound_locs = np.random.choice(range(len(bound_points)), round(m * bound_prop), replace=False)
    points = [bound_points[bl] for bl in bound_locs] + [remain_points[rl] for rl in remain_locs]
    
    H1_lin_comb = np.zeros((H1.n, m))
    L2_lin_comb = np.zeros((L2.n, m))
    local_meas_fun = np.zeros((L2.side_len, L2.side_len))
    for i in range(m):
        point = points[i]

        meas = np.zeros((L2.side_len, L2.side_len))
        meas[point[0]:point[0]+width,point[1]:point[1]+width] = 1.0 

        L2_lin_comb[:, i] = meas.flatten()
        H1_lin_comb[:, i] = H1.riesz_rep_coeffs(hil.int_L2_H1_basis(meas.flatten(), L2), normalise=False)
        local_meas_fun[point[0]:point[0]+width,point[1]:point[1]+width] += 1.0

    W = hil.Basis(H1_lin_comb, space=H1)
    W_L2 = hil.Basis(L2_lin_comb, space=L2)
    if return_map:
        return W, W_L2, local_meas_fun

    return W

def make_local_avg_grid_basis(width_div, spacing_div, div, return_map=False):

    if div < spacing_div or spacing_div < width_div or width_div < 1:
        raise Exception('Require 1 <= width < spacing < div')
   
    H1 = hil.H1DyadicSq(div)
    L2 = hil.L2DyadicSq(div)

    spacing = 2**spacing_div
    width = 2**width_div

    h = 2**(-div)
    meas_grid = 2**(div - spacing_div)

    H1_lin_comb = np.zeros((H1.n, meas_grid * meas_grid))
    L2_lin_comb = np.zeros((L2.n, meas_grid * meas_grid))
    local_meas_fun = np.zeros((L2.side_len, L2.side_len))
    
    for i in range(meas_grid):
        for j in range(meas_grid):

            i_start = i * spacing + spacing//2 - width//2
            j_start = j * spacing + spacing//2 - width//2

            meas = np.zeros((L2.side_len, L2.side_len))
            meas[i_start:i_start+width,j_start:j_start+width] = 1.0
            
            L2_lin_comb[:, j + i*meas_grid] = meas.flatten() 
            H1_lin_comb[:, j + i*meas_grid] = H1.riesz_rep_coeffs(hil.int_L2_H1_basis(meas.flatten(), L2), normalise=False)

            local_meas_fun[i_start:i_start+width, j_start:j_start+width] += 1
    
    W = hil.Basis(H1_lin_comb, space=H1)
    W_L2 = hil.Basis(L2_lin_comb, space=L2)
    if return_map:
        return W, W_L2, local_meas_fun

    return W

