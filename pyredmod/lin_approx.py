"""
lin_approx.py

Author: James Ashton Nichols
Start date: December 2018

Linear approximator that does the Maday-Patera reconstruction to a linear space
"""

import math
import numpy as np
import scipy.linalg
import copy
import warnings

import pyhilbert as hil

class LinearWorstCaseEstimator(object):
    """ This class automatically sets up the cross grammian, calculates
        beta, and can do the optimal reconstruction and calculate a favourable basis """

    def __init__(self, Vn, center=None, Wm=None):
        if center is None:
            self.center = hil.Vector.zero(space=Vn.space)
        else:
            self.center = center

        self._Vn = None
        self._Wm = None
        self._CG = None 
        self._solver = None
        self._Wm_tilde = self._gamma = self._Vn_tilde = None
         
        self.Vn = Vn
        self.Wm = Wm
    
    @property
    def n(self):
        if isinstance(self.Vn, hil.Vector):
            return 1
        else:
            return self.Vn.n
        
    @property
    def m(self):
        if isinstance(self.Wm, hil.Vector):
            return 1
        else:
            return self.Wm.n

    @property
    def Wm(self):
        return self._Wm
    @Wm.setter
    def Wm(self, new_Wm):
        
        if self._Wm is None or not new_Wm == self._Wm:
            if new_Wm is not None:
                if not new_Wm.is_orthonormal:
                    raise Exception( f'{self.__class__.__name__}: Wm must orthonormal '
                                      'for linear reconstruction')

            if self._Wm is not None:
                warnings.warn(f'{self.__class__.__name__}: Setting new Wm space')
                if self.Vn.n > new_Wm.n:
                    raise Exception('Error - Wm must be of higher dimensionality '
                                    'than Vn to be able to do optimal reconstruction')
            self._Wm = new_Wm
            self._CG_is_stale = True
            if self._Wm is not None:
                self.P_W_center, self.P_W_center_coords = self._Wm.project(self.center, return_coeffs=True)
            
    @property
    def Vn(self):
        return self._Vn
    @Vn.setter
    def Vn(self, new_Vn):
        if new_Vn is not self._Vn:
            if new_Vn is not None:
                if not new_Vn.is_orthonormal:
                    raise Exception( f'{self.__class__.__name__}: Vn must be orthonormal '
                                      'for linear reconstruction')
        
            if self._Vn is not None:
                warnings.warn(f'{self.__class__.__name__}: Setting new Vn space')
                if self.Wm is not None:
                    if new_Vn.n > self.Wm.n:
                        raise Exception('Error - Wm must be of higher dimensionality '
                                    'than Vn to be able to do optimal reconstruction')

            self._Vn = new_Vn
            self._CG_is_stale = True


    @property
    def CG(self):
        self._update_CG()
        return self._CG

    def _update_CG(self):
        if self.Wm is not None:
            self._CG = self.Wm.A @ self.Vn
            self._CG_is_stale = False
            self.r = np.linalg.matrix_rank(self._CG)
        else:
            raise Exception( f'{self.__class__.__name__}: No measurement space Wm set!')
 
    def _calc_svd(self):
        self._Wm_tilde, self._gamma, self._Vn_tilde = np.linalg.svd(self.CG)
        self._Vn_tilde = self._Vn_tilde.T

    def _w_mf_coords(self, w_coords):        
        w_mf_coords = (w_coords.T - self.P_W_center_coords).T
        if self.Wm.n == 1 and w_mf_coords.shape[0] != 1:
            return np.atleast_2d(w_mf_coords)
        return w_mf_coords

    def append_Vn(self, v):
        self.Vn.append(v)
        self._CG_is_stale = True
        self._CG = np.pad(self._CG, ((0,0),(0,1)), 'constant')

        self._Wm_tilde = self._gamma = self._Vn_tilde = None

    def append_Wm(self, w):
        self.Wm.append(w)
        self._CG_is_stale = True
        self._CG = np.pad(self._CG, ((0,1),(0,0)), 'constant')

        self._Wm_tilde = self._gamma = self._Vn_tilde = None

    def __getitem__(self, idx):
        return type(self)(self.Vn[idx], center=self.center, Wm=self.Wm)

    def beta(self):
        if self.m < self.n:
            return 0.0
            
        if self._gamma is None:
            self._gamma = scipy.linalg.svd(np.atleast_2d(self.CG), compute_uv=False)

        return self._gamma[-1]

    def Wm_singular_vec(self, index):
        self._calc_svd()
        return self.Wm @ self._Wm_tilde[:, index] 

    def Vn_singular_vec(self, index):
        self._calc_svd()
        return self.Vn @ self._Vn_tilde[:, index]

    def measure_and_reconstruct(self, u):
        """ Just a little helper function. Not sure we really want this here """ 
        u_p_W = self.Wm.A @ u
        return self.best_estimate(u_p_W)
    
    def best_estimate_coords(self, w_coords):
        w_mf_coords = self._w_mf_coords(w_coords)
        try:
            # Note the transpose: this is so if w is a collection of coords,
            # the broadcast subtraction of P_W_center_coords is done correctly
            c = scipy.linalg.solve(self.CG.T @ self.CG, self.CG.T @ w_mf_coords, 
                                   sym_pos=True)
        except np.linalg.LinAlgError as e:
            warnings.warn(f'Unstable v* calculation, m={self.m}, n={self.n} '
                          'for Wm and Vn, returning 0 function')
            c = np.zeros(self.Vn.n)
        
        return c

    def best_estimate(self, w_coords):
        """ And here it is - the optimal reconstruction """
        c = self.best_estimate_coords(w_coords)
        
        # v_star is the nearest best estimate in the span of Vn
        # v_star_W is the coordinates of the projection of v_star in W
        if isinstance(self.Vn, hil.Vector):
            v_star = hil.Basis(self.Vn.values, space=self.Vn.space) * c + self.center
            v_star_W = np.squeeze((self.CG * np.atleast_2d(c).T + self.P_W_center_coords).T)
        else:
            v_star = self.Vn @ c + self.center
            v_star_W = ((self.CG @ c).T + self.P_W_center_coords).T
        
        u_star = v_star + self.Wm @ (w_coords - v_star_W)

        return u_star
