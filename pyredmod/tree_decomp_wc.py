"""
tree_decomp_mixture.py

Author: James Ashton Nichols
Start date: September 2018

A class to do encapsulate a local PCA basis
"""

import numpy as np
import copy
import itertools

import warnings

import pyhilbert as hil

# Now we need some tree structure to keep track of the splitting tree    
from anytree import NodeMixin, RenderTree, LevelOrderIter, LevelOrderGroupIter

from pyredmod.pyredmod.pw_lin_approx import *
from pyredmod.pyredmod.tree_pw_lin_approx import *
from pyredmod.pyredmod.tree_decomp_mixture import *
from pyredmod.pyredmod.greedy_methods import *

class ParamDecompTreeWC(NodeMixin):
    
    def __init__(self, U, f, param_range, est_n=50, levels=[0], min_N=100, parent=None, affine=False, Wm=None):
        
        # U is assumed to be of type ParamBasis, that is it has its parameters
        # attached to its vectors
        self.U = U
        
        self.param_range = param_range
        self.d = param_range.shape[0]
        self.est_n = est_n

        self.func = f
        self._min_N = min_N
        self.affine = affine
        self.Wm = Wm

        self.levels = levels
        
        self._make_local_wc_est(U)
        
        self.name = f'Local linear est on params {self.param_range.tolist()}, dim {self.lin_est.n}'
        self.parent = parent
        
    def __str__(self):
        return self.name
    
    @property
    def selected_params(self):
        self._mask = self.U.in_param_range(self.param_range)
        return self.U.params[self._mask]
    @property
    def selected_U(self):
        self._mask = self.U.in_param_range(self.param_range)
        if self._mask.sum() > 0:
            return self.U.basis[self._mask]
        else:
            return None

    def _make_local_wc_est(self, U):
        self._mask = U.in_param_range(self.param_range)

        if self._mask.sum() < self._min_N:
            # We need to beef up the local sample collection if it is below the threshold
            # of sample numbers (min_N)
            params_new = np.random.random((self._min_N - self._mask.sum(), self.d)) \
                        * (self.param_range[:, 1] - self.param_range[:, 0]) + self.param_range[:, 0]
            U_new = self.func(params_new)
            self.U.append(U_new, params_new)
            self._mask = U.in_param_range(self.param_range)
       
        if self.affine:
            # We take the center point to be the center of the parameter range
            #center = self.func(self.param_range.mean(axis=1))
            center = self.selected_U @ (np.ones(self.selected_U.n) / self.selected_U.n)
            greedy = GreedyApprox(self.selected_U - center, verbose=False)
        else:
            greedy = GreedyApprox(self.selected_U, verbose=False)

        if self.Wm is None:
            Vn = greedy.construct_to_n(self.est_n+1)[:-1]
            self.residual = greedy.residuals[-1]
            self.split_criteria = self.residual
        else:
            Vn, self.residual, self.inf_sup = greedy.construct_to_poor_mans_criteria(self.Wm)
            self.split_criteria = self.residual / self.inf_sup
       
        Vn_ortho = Vn.orthonormalise()

        if self.affine:
            self.lin_est = LinearWorstCaseEstimator(Vn_ortho, center, Wm=self.Wm)
        else:
            self.lin_est = LinearWorstCaseEstimator(Vn_ortho, Wm=self.Wm)


    def split_leaf(self, directions=None, choose_big_leaf=False):
        if not self.is_root:
            raise ValueError(f'{self.__class__.__name__}: must be tree root to '
                             f'split leaves')
        
        if not self.descendants:
            self.levels.append(self.levels[-1] + 1)
            self.split(directions)
        else:
            if not choose_big_leaf:
                for i, node in enumerate(self.leaves):
                    node.split(directions)
            else:
                split_criteria = np.array([node.split_criteria for node in self.leaves])
                split_node = np.argmax(split_criteria)

                for node in self.leaves:
                    node.levels.append(node.levels[-1]+1)
                if directions is None:
                    self.leaves[split_node].split(directions='max')
                else:
                    self.leaves[split_node].split(directions)

    @property
    def leaves(self):
        self._leaves = []
        if self.descendants:
            # Now rebuild the leaves list and the estimator
            for i, node in enumerate(self.descendants):
                if node.is_leaf:
                    self._leaves.append(node)
        else:
            self._leaves = [self]

        return self._leaves

    def split(self, directions=None):
        
        if directions == 'max':
            # first make candidate children
            self.generate_candidate_children()
            # choose the direction that has the smallest resulting residual
            split_criteria = np.array([[cand_lo.split_criteria, cand_hi.split_criteria] for cand_lo, cand_hi in zip(self.candidate_child_lo, self.candidate_child_hi)])

            # For now we choose the lowest SUM of the residuals
            #direction = np.argmin(split_criteria.sum(axis=-1))
            direction = np.argmin(split_criteria.max(axis=-1))
            self.candidate_child_lo[direction].parent = self
            self.candidate_child_hi[direction].parent = self
            self.levels = self.levels[:-1]

            del self.candidate_child_lo
            del self.candidate_child_hi
        else:    
            if directions is None:
                directions = list(range(self.d))

            combo in itertools.product(*(len(directions)*[[1, 0]]))

            split_params = copy.copy(self.param_range)
            for i, dim in enumerate(directions):
                split_params[dim][combo[i]] = split_params[dim].mean()
                split_child = ParamDecompTreeWC(self.U, self.func, split_params, est_n=self.est_n, \
                    levels=[self.levels[-1]], min_N=self._min_N, parent=self, affine=self.affine)
 
    def generate_candidate_children(self):
        # Loop through all d dimensions, do the splitting (by creating) and save
        
        self.candidate_child_lo = []
        self.candidate_child_hi = []
        for i in range(self.d):

            param_range_l = np.copy(self.param_range)
            param_range_l[i,1] = self.param_range[i,:].mean()
            param_range_r = np.copy(self.param_range)
            param_range_r[i,0] = self.param_range[i,:].mean()

            # The children are set up with no parent at first. They are "detached" for now. 
            # The ones we choose will become attached
            self.candidate_child_lo.append(ParamDecompTreeWC(self.U, self.func, param_range_l, est_n=self.est_n, \
                                            levels=[self.levels[-1]], min_N=self._min_N, parent=None, affine=self.affine, Wm=self.Wm))
            self.candidate_child_hi.append(ParamDecompTreeWC(self.U, self.func, param_range_r, est_n=self.est_n, \
                                            levels=[self.levels[-1]], min_N=self._min_N, parent=None, affine=self.affine, Wm=self.Wm))

    @property
    def oracle_estimator(self):
        # We match the param to the
        leaf_ests = []
        leaf_param_ranges = []
        
        if self.is_root:
            for i, node in enumerate(self.leaves):
                leaf_ests.append(node.lin_est)
                leaf_param_ranges.append(node.param_range)

            self._oracle_estimator = PWOracleEstimator(leaf_ests, leaf_param_ranges, Wm=self.Wm)
            return self._oracle_estimator

        else:            
            raise ValueError(f'{self.__class__.__name__}: must be tree root to '
                             f'make estimator')

    @property
    def tree_estimator(self):
        # We match the param to the
        ests = []
        param_ranges = []
        levels = []
        resids = []
        inf_sups = []

        if self.is_root:
            for i, node in enumerate(LevelOrderIter(self)):
                ests.append(node.lin_est)
                param_ranges.append(node.param_range)
                levels.append(node.levels)
                resids.append(node.residual)
                inf_sups.append(node.inf_sup)

            self._tree_estimator = TreePWEstimator(ests, param_ranges, levels, residuals=np.array(resids), Wm=self.Wm)
            return self._tree_estimator

        else:            
            raise ValueError(f'{self.__class__.__name__}: must be tree root to '
                             f'make estimator')
