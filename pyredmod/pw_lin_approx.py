"""
pw_lin_approx.py

Author: James Ashton Nichols
Start date: June 2019
"""

import numpy as np
import warnings

_RESID_TOL = 1e-6
_EM_TOL = 1.0 
 
class PWOracleEstimator(object):
    """
    A piece-wise estimator that selects a local estimator using some criteria. 
    Can be used (notionally) with a linear or gaussian estimator.
    """
    def __init__(self, estimators, param_ranges, Wm=None):
        
        if len(estimators) != len(param_ranges):
            raise ValueError(f'{self.__class__.__name__}: each estimator needs '
                              'exactly one parameter set')

        if estimators is not None:
            self._estimators = estimators
        if param_ranges is not None:
            self._param_ranges = param_ranges
        
        self._Wm = None
        self.Wm = Wm
        
    @property
    def Wm(self):
        return self._Wm
    @Wm.setter
    def Wm(self, new_Wm):
        if new_Wm is not None:
            if not new_Wm.is_orthonormal:
                raise Exception( f'{self.__class__.__name__}: Wm must be an orthonormal ' 
                                  'basis for correct reconstruction')
            if self._Wm is None or not new_Wm == self._Wm:
                if self._Wm is not None:
                    warnings.warn(f'{self.__class__.__name__}: Setting new Wm space')
    
                self._Wm = new_Wm
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    for est in self.estimators:
                        est.Wm = new_Wm
    
    @property
    def estimators(self):
        return self._estimators

    @estimators.setter
    def estimators(self, new_est):
        self._estimators = new_est
    
    def __getitem__(self, idx):
        return (self._estimators[idx], self._param_ranges[idx])

    def __setitem__(self, idx, estimator_and_param_ranges):
        self._estimators[idx] = estimator_and_param_ranges[0]
        self._param_ranges[idx] = estimator_and_param_ranges[1]
        if self.Wm:
            self._estimators[idx].Wm = self.Wm

    def __delitem__(self, idx):
        del self._estimators[idx]
        del self._param_ranges[idx]

    def append(self, estimator_and_param_ranges):
        if self.Wm:
            estimator_and_param_ranges[0].Wm = self.Wm
        self._estimators.append(estimator_and_param_ranges[0])
        self._param_ranges.append(estimator_and_param_ranges[1])

    def __iter__(self):
        return zip(self._estimators, self._param_ranges)

    def truncate(self, idx):
        return type(self)([est[idx] for est in self.estimators], self._param_ranges, Wm=self.Wm)

    def in_param_range(self, param):
        sel = []
        for i, (est, param_range) in enumerate(self):
            if all([pr[0] <= p and p <= pr[1] for p, pr in zip(param, param_range)]):
                sel.append(i)
        return sel 
    
    def best_estimate(self, w_coords, u, return_index=False):
        # This routine finds the linear estimator that has the best approximation
        # of the lot, 
        best_est = None

        ests = [est.best_estimate(w_coords) for est in self._estimators]
        errs = np.array([(est - u).norm() for est in ests])

        if return_index:
            return ests[np.argmin(errs)], np.argmin(errs)

        return ests[np.argmin(errs)]

    def best_param_matching_estimate(self, w_coords, param, return_index=False):
        # This routine finds the linear estimator that has a parameter range that
        # contains the parameter of the funtion to be reconstructed, and uses it
        # to do an optimal reconstruction

        best_est = None

        sel = self.in_param_range(param)
        if len(sel) > 0:
            best_est = self._estimators[sel[0]]

        if best_est is None:
            warnings.warn(f'{self.__class__.__name__}: no estimator found containing param {param}')
        
        if return_index:
            return best_est.best_estimate(w_coords), sel[0]

        return best_est.best_estimate(w_coords)
      
    def best_guess_estimate(self, w_coords, resid_guesser, solver, return_index=False):
        # For each linear estimator
        # ... Make an estimate
        # ... Guess the coordinates using the guesser
        # Then see which of the distances is smallest
       
        y_guesses = np.zeros((len(self._estimators), self._param_ranges[0].shape[0]))
        u_stars = []
        u_guesses = []
        u_guess_star_diff = np.zeros((len(self._estimators)))
        for k, (est, param_range) in enumerate(self):
            u_stars.append(est.best_estimate(w_coords))
            y_guesses[k], residual = resid_guesser.nearest_params_resid(u_stars[-1], y_bounds=param_range)
            u_guesses.append(solver(y_guesses[k])[0])
            u_guess_star_diff[k] = (u_stars[-1] - u_guesses[-1]).norm()

        est_choice = np.argmin(u_guess_star_diff)

        if return_index:
            return u_stars[est_choice], y_guesses[est_choice], u_guesses[est_choice], est_choice

        return u_stars[est_choice], y_guesses[est_choice], u_guesses[est_choice]

    def best_resid_estimate(self, w_coords, resid_guesser, solver, return_index=False):
        # For each linear estimator
        # ... Make an estimate
        # Then see which of the residual distances is smallest
         
        y_guesses = np.zeros((len(self._estimators), self._param_ranges[0].shape[0]))
        u_stars = []
        u_guesses = []
        residuals = np.zeros((len(self._estimators)))
        for k, (est, param_range) in enumerate(self):
            u_stars.append(est.best_estimate(w_coords))
            y_guesses[k], residual = resid_guesser.nearest_params_resid(u_stars[-1], y_bounds=param_range)
            u_guesses.append(solver(y_guesses[k]))
            residuals[k] = residual

        est_choice = np.argmin(residuals)

        if return_index:
            return u_stars[est_choice], y_guesses[est_choice], u_guesses[est_choice], est_choice

        return u_stars[est_choice], y_guesses[est_choice], u_guesses[est_choice]

