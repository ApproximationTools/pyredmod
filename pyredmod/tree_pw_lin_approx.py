"""
pw_lin_approx.py

Author: James Ashton Nichols
Start date: June 2019
"""

import numpy as np
import warnings

import pyhilbert as hil

_RESID_TOL = 1e-6
_EM_TOL = 1.0 
 
class TreePWEstimator(object):
    """
    A piece-wise estimator that selects a local estimator using some criteria,
    but also retains knowledge of the tree decomposition used to generate this. 
    Can be used (notionally) with a linear or gaussian estimator.
    """
    def __init__(self, estimators, param_ranges, levels, residuals=None, Wm=None):
        
        if len(estimators) != len(param_ranges):
            raise ValueError(f'{self.__class__.__name__}: each estimator needs '
                              'exactly one parameter set')

        self._estimators = estimators
        self._param_ranges = param_ranges
        self._levels = levels
        self._residuals = residuals

        self._Wm = None
        self.Wm = Wm

        self.estimates = self.guesses = None
        
    @property
    def Wm(self):
        return self._Wm
    @Wm.setter
    def Wm(self, new_Wm):
        if new_Wm is not None:
            if not new_Wm.is_orthonormal:
                raise Exception( f'{self.__class__.__name__}: Wm must be an orthonormal ' 
                                  'basis for correct reconstruction')
            if self._Wm is None or not new_Wm == self._Wm:
                if self._Wm is not None:
                    warnings.warn(f'{self.__class__.__name__}: Setting new Wm space')
    
                self._Wm = new_Wm
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    for est in self.estimators:
                        est.Wm = new_Wm
    
    @property
    def estimators(self):
        return self._estimators

    @estimators.setter
    def estimators(self, new_est):
        self._estimators = new_est
    
    def __getitem__(self, idx):
        return (self._estimators[idx], self._param_ranges[idx])

    def __setitem__(self, idx, estimator_and_param_ranges):
        self._estimators[idx] = estimator_and_param_ranges[0]
        self._param_ranges[idx] = estimator_and_param_ranges[1]
        if self.Wm:
            self._estimators[idx].Wm = self.Wm

    def __delitem__(self, idx):
        del self._estimators[idx]
        del self._param_ranges[idx]

    def append(self, estimator_and_param_ranges):
        if self.Wm:
            estimator_and_param_ranges[0].Wm = self.Wm
        self._estimators.append(estimator_and_param_ranges[0])
        self._param_ranges.append(estimator_and_param_ranges[1])

    def __iter__(self):
        return zip(self._estimators, self._param_ranges)

    def truncate(self, idx):
        return type(self)([est[idx] for est in self.estimators], self._param_ranges, Wm=self.Wm)

    def in_param_range(self, param):
        sel = []
        for i, (est, param_range) in enumerate(self):
            if all([pr[0] <= p and p <= pr[1] for p, pr in zip(param, param_range)]):
                sel.append(i)
        return sel
        
    def estimates_full_tree(self, w_coords, us, params, solver, guesser):

        self.estimates = []
        self.guesses = []
        self.param_guesses = np.zeros((len(self._estimators), us.n, np.atleast_2d(params).shape[1]))
        self.estimate_errors = np.zeros((len(self._estimators), us.n))
        self.estimate_residuals = np.zeros((len(self._estimators), us.n))
        self.guess_errors = np.zeros((len(self._estimators), us.n))
        self.in_param_range = np.zeros((len(self._estimators), us.n), dtype=np.bool)
        
        for k, (est, param_range) in enumerate(zip(self._estimators, self._param_ranges)):
            u_stars = est.best_estimate(w_coords)
            self.estimates.append(u_stars)
            self.estimate_errors[k,:] = (u_stars - us).norms()

            self.in_param_range[k,:] = np.all(param_range.T[0] <= params, axis=1) & np.all(params < param_range.T[1], axis=1)

            y_guesses = np.zeros((us.n, params.shape[1]))
            u_guesses = []
            u_guesses_diff = np.zeros(us.shape)

            for i, u_star in enumerate(u_stars):
                self.param_guesses[k, i, :], self.estimate_residuals[k, i] = guesser.nearest_params_resid(u_star, y_bounds=param_range)
                u_guesses.append(solver(self.param_guesses[k, i, :])[0])
            
            self.estimates.append(u_stars)
            self.guesses.append(hil.Basis(u_guesses))
            self.guess_errors[k, :] = (us - self.guesses[-1]).norms()
        
    def at_level(self, level):
        # We take a particular level of the tree
        include_ests = []
        for i, (est, levels) in enumerate(zip(self._estimators, self._levels)):
            if level in levels:
                include_ests.append(i)

        return include_ests
    
    def best_estimates(self, level):
        level_sel = self.at_level(level)
        ests = [self.estimates[l] for l in level_sel]
        errs = self.estimate_errors[level_sel]
        argmins = np.argmin(errs, axis=0)

        best_ests = hil.Basis(np.zeros(ests[0].values.shape), self.estimates[0].space)
        for i in range(best_ests.n):
            best_ests[i] = ests[argmins[i]][i]
        
        return best_ests, argmins, errs.min(axis=0)

    def best_param_matching_estimates(self, level):
        level_sel = self.at_level(level)
        ests = [self.estimates[l] for l in level_sel]
        in_range = self.in_param_range[level_sel]
        if np.any(in_range.sum(axis=0) > 1):
            import pdb; pdb.set_trace()

        matching_param_range = np.zeros(ests[0].n, dtype=np.int)
        match_ests = hil.Basis(np.zeros(ests[0].values.shape), self.estimates[0].space)
        for i in range(match_ests.n):
            matching_param_range[i] = np.where(in_range[:,i])[0][0]
            match_ests[i] = ests[matching_param_range[i]][i]

        return match_ests, matching_param_range
      
    def best_guess_estimates(self, level):
        level_sel = self.at_level(level)
        guess = [self.guesses[l] for l in level_sel]
        guess_errs = self.guess_errors[level_sel]
        argmins = np.argmin(guess_errs, axis=0)

        best_guesses = hil.Basis(np.zeros(guess[0].values.shape), self.estimates[0].space)
        for i in range(best_guesses.n):
            best_guesses[i] = guess[argmins[i]][i]
        
        return best_guesses, argmins, guess_errs.min(axis=0)

    def best_resid_estimates(self, level):
        level_sel = self.at_level(level)
        ests = [self.estimates[l] for l in level_sel]
        errs = self.estimate_errors[level_sel]
        resids = self.estimate_residuals[level_sel]
        argmins = np.argmin(resids, axis=0)

        resid_ests = hil.Basis(np.zeros(ests[0].values.shape), self.estimates[0].space)
        resid_errs = np.zeros(resids.shape[1])
        for i in range(resid_ests.n):
            resid_ests[i] = ests[argmins[i]][i]
            resid_errs[i] = errs[argmins[i],i]
        
        return resid_ests, argmins, resid_errs 
