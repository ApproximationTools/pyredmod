"""
tree_decomp_mixture.py

Author: James Ashton Nichols
Start date: September 2018

A class to do encapsulate a local PCA basis
"""

import numpy as np
import copy
import warnings
import anytree

import pyhilbert as hil
from pyredmod.pyredmod.gauss_approx import *
from pyredmod.pyredmod.mixture_approx import *

def W2_gaussians(mu, nu):
    # Calculates the Wasserstein-2 / Kantorovich metric between two Gaussians
    # TODO: see if there's anyway to make the rotation_adj quicker
    CG = mu.Phi.A @ nu.Phi
    
    mean_diff = (nu.mean - mu.mean).norm() ** 2
    tot_trace = mu.sigma_sq.sum() + nu.sigma_sq.sum()
    rot_adj = np.real(2 * np.trace(sp.linalg.sqrtm(np.diag(np.sqrt(mu.sigma_sq)) \
                    @ CG @ np.diag(nu.sigma_sq) \
                    @ CG.T @ np.diag(np.sqrt(mu.sigma_sq)))))
    return np.sqrt(mean_diff + tot_trace - rot_adj)

class ParamBasis(object):
    """ Just a class that has the parameters associated with the basis at the same time,
        so that we maintain a static set over the tree of approximators """
    #TODO Make a static factory constructor from samples, space, and ys
    def __init__(self, basis, params):
        
        assert params.shape[0] == basis.n
        self.params = params 
        self.basis = basis
    
    def append(self, other, params):
        self.basis.append(other)
        self.params = np.vstack((self.params, params))
    
    def in_param_range(self, param_range):
        return np.all((self.params >= param_range[:,0]) & (self.params < param_range[:,1]), axis=-1)

class ParamDecompTree(anytree.NodeMixin):
    
    def __init__(self, U, f, param_range, Wm=None, level=1, min_N=100, parent=None):
        
        # U is assumed to be of type ParamBasis, that is it has its parameters
        # attached to its vectors
        self.U = U
        self.U.basis.update_G()
        
        self.param_range = param_range
        self.d = param_range.shape[0]

        self.func = f
        self._min_N = min_N

        self.level = level
        self._make_local_gaussian(U)

        self.prob = (self.param_range[:, 1] - self.param_range[:,0]).prod()
        
        self.parent = parent
        
        self.split_dir = None
        self.candidate_child_lo = None
        self.candidate_child_hi = None
        
        self.name = f'Local Gaussian on params {self.param_range.tolist()}, level {self.level}, prob ' \
                    f'{self.prob:.3e}, tot var {self.gauss_est.sigma_sq.sum():.3e}'
        
    def __str__(self):
        return self.name
  
    @property
    def selected_params(self):
        self._mask = self.U.in_param_range(self.param_range)
        return self.U.params[self._mask]
    @property
    def selected_U(self):
        self._mask = self.U.in_param_range(self.param_range)
        return self.U.basis[self._mask]

    def _make_local_gaussian(self, U):
        self._mask = U.in_param_range(self.param_range)

        if self._mask.sum() < self._min_N:
            # We need to beef up the local sample collection if it is below the threshold
            # of sample numbers (min_N)
            params_new = np.random.random((self._min_N - self._mask.sum(), self.d)) \
                        * (self.param_range[:, 1] - self.param_range[:, 0]) + self.param_range[:, 0]
            U_new = self.func(params_new)
            self.U.append(U_new, params_new)
            self._mask = U.in_param_range(self.param_range)

        self.gauss_est = GaussianEstimator.from_samples(self.selected_U)

    def variance(self):
        return self.gauss_est.variance
    def variance_sq(self):
        return self.gauss_est.variance_sq
    def residual_sq(self):
        return self.gauss_est.sigma_sq[self.d:].sum()
                      
    def generate_candidate_children(self):
        # Loop through all d dimensions, do the splitting (by creating) and save
        if self.candidate_child_lo is None or self.candidate_child_hi is None:
            
            self.candidate_child_lo = []
            self.candidate_child_hi = []
            for i in range(self.d):

                param_range_l = np.copy(self.param_range)
                param_range_l[i,1] = self.param_range[i,:].mean()
                param_range_r = np.copy(self.param_range)
                param_range_r[i,0] = self.param_range[i,:].mean()

                # The children are set up with no parent at first. They are "detached" for now. 
                # The ones we choose will become attached
                self.candidate_child_lo.append(ParamDecompTree(self.U, self.func, param_range_l, \
                                                level=self.level+1, min_N=self._min_N, parent=None))
                self.candidate_child_hi.append(ParamDecompTree(self.U, self.func, param_range_r, \
                                                level=self.level+1, min_N=self._min_N, parent=None))

    def split_leaf(self, split_method = 'W2'):
        if not self.is_root:
            raise ValueError(f'{self.__class__.__name__}: must be tree root to '
                             f'split leaves')

        if not self.descendants:
            leaf_to_split = 0
        else:
            split_crits = []

            for i, node in enumerate(self.leaves):
                if split_method == 'W2':
                    W2_dist = node.children_W2_dist()
                    split_crits.append(node.prob * W2_dist.max())
                elif split_method == 'resid':
                    #resids = node.children_resid()
                    split_crits.append(node.prob * node.residual_sq())
                    #print(f'leaf {i}:', node.gauss_est.sigma_sq, node.gauss_est.sigma_sq.sum())
                elif split_method == 'tot_var':
                    #resids = node.children_resid()
                    split_crits.append(node.prob * node.variance_sq())
                    #print(f'leaf {i}:', node.gauss_est.sigma_sq, node.gauss_est.sigma_sq.sum())
            
            #print('Cells resids:', split_crits)

            leaf_to_split = np.array(split_crits).argmax()
             
        self.leaves[leaf_to_split].split(split_method=split_method)
        self._leaves = None
        self._mixture_estimator = None

    @property
    def leaves(self):
        self._leaves = []
        if self.descendants:
            # Now rebuild the leaves list and the estimator
            for i, node in enumerate(self.descendants):
                if node.is_leaf:
                    self._leaves.append(node)
        else:
            self._leaves = [self]

        return self._leaves

    def children_W2_dist(self):
        self.generate_candidate_children()

        W2_dist = np.zeros(self.d)
        for i, (child_lo, child_hi) in enumerate(zip(self.candidate_child_lo, self.candidate_child_hi)):
            W2_dist[i] = W2_gaussians(child_lo.gauss_est, child_hi.gauss_est)

        return W2_dist

    def children_resid(self):
        self.generate_candidate_children()

        resids = np.zeros(self.d)
        for i, (child_lo, child_hi) in enumerate(zip(self.candidate_child_lo, self.candidate_child_hi)):
            resids[i] = child_lo.residual_sq() + child_hi.residual_sq()

        return resids
    
    def children_tot_vars(self):
        self.generate_candidate_children()

        tot_vars = np.zeros(self.d)
        for i, (child_lo, child_hi) in enumerate(zip(self.candidate_child_lo, self.candidate_child_hi)):
            tot_vars[i] = child_lo.variance_sq() + child_hi.variance_sq()

        return tot_vars 

    def split(self, direction=None, split_method='W2'):
        
        if direction:
            self.split_dir = direction

        elif split_method == 'W2':
            W2_dists = self.children_W2_dist()
            self.split_dir = np.argmax(W2_dists)
        elif split_method == 'resid':
            resids = self.children_resid()
            self.split_dir = np.argmax(resids)
            #for i, (child_lo, child_hi) in enumerate(zip(self.candidate_child_lo, self.candidate_child_hi)):
            #    print('split dir i', child_lo.gauss_est.sigma_sq, child_lo.gauss_est.sigma_sq.sum())
            #    print('           ', child_hi.gauss_est.sigma_sq, child_hi.gauss_est.sigma_sq.sum())

            #print('split dir resids', resids)
        elif split_method == 'tot_var':
            tot_vars = self.children_tot_vars()
            self.split_dir = np.argmin(tot_vars)
            #for i, (child_lo, child_hi) in enumerate(zip(self.candidate_child_lo, self.candidate_child_hi)):
            #    print('split dir i', child_lo.gauss_est.sigma_sq, child_lo.gauss_est.sigma_sq.sum())
            #    print('           ', child_hi.gauss_est.sigma_sq, child_hi.gauss_est.sigma_sq.sum())
        
        self.candidate_child_lo[self.split_dir].parent = self
        self.candidate_child_hi[self.split_dir].parent = self

        #self.child_lr[0].parent = self.child_lr[1].parent = self
        self.name += ', split in dim ' + str(self.split_dir)
        
        del self.candidate_child_lo
        del self.candidate_child_hi
    
    @property
    def mixture_estimator(self):
        leaf_ests = []
        leaf_probs = []

        for i, node in enumerate(self.leaves):
            leaf_ests.append(node.gauss_est)
            leaf_probs.append(node.prob / self.prob)

        self._mixture_estimator = MixtureEstimator(leaf_ests, leaf_probs)
        return self._mixture_estimator

    def best_estimate(self, w_coords, Wm):
        mix_est = self.mixture_estimator
        mix_est.Wm = Wm
        return mix_est.best_estimate(w_coords)

