"""
gauss_approx.py

Author: James Ashton Nichols
Start date: December 2018

A Gaussian distribution model that has tools to do optimal reconstruction
from measurement, and construction of the gaussian from data using PCA
"""

import math
import numpy as np
import scipy as sp
import copy

import warnings

import pyhilbert as hil

_RESID_TOL = 1e-6
_EIG_TOL = 1e-12
# TODO: Really should re-consider the whole pattern of this class. How do we do
# minimum fuss sub-indexing, access to the cross-grammians, with fast performance?

def pca_from_samples(U, weights=None, eig_tol=_EIG_TOL):
    # This is a PCA routine for arbitrary Hilbert spaces, with an optional
    # weighted form that is used in the Expectation-Maximisation algorithm.

    if weights is None:
        norm_weights = np.ones(U.n) / U.n
    else:
        if weights.sum() <= _RESID_TOL:
            return None
        norm_weights = weights / weights.sum()
    
    u_mean = U @ norm_weights
    U_mf = U - u_mean
    
    sig_sq, V = sp.linalg.eigh((U_mf.G.T * np.sqrt(norm_weights)).T * np.sqrt(norm_weights))

    V = V[:,::-1]
    sig_sq = sig_sq[::-1] 
    V = V[:,sig_sq > eig_tol]
    sig_resid_sq = sig_sq[sig_sq <= eig_tol]
    sig_sq = sig_sq[sig_sq > eig_tol]
    
    PCA_Basis = U_mf @ ((np.sqrt(1.0/sig_sq) * V).T * np.sqrt(norm_weights)).T
    return PCA_Basis, sig_sq, u_mean, np.sqrt(np.abs(sig_resid_sq).sum())

class GaussianEstimator(object):

    def __init__(self, mean, Phi, sigma_sq, Wm=None, resid_tol=_RESID_TOL):
        self.mean = mean
        self._Phi = Phi
        self._sigma_sq = sigma_sq
        self._sigma = np.sqrt(sigma_sq)
        self._Sigma = np.diag(np.sqrt(sigma_sq))
        
        self._resid_tol = resid_tol

        self._GW = None
        self._GW_is_stale = True
        self._solver = None
        self._Wm = None
        self.Wm = Wm

    @classmethod
    def from_samples(cls, U, weights=None, eig_tol=_EIG_TOL):
        Phi, sigma_sq, mean, resid_tol = pca_from_samples(U, weights=weights, eig_tol=_EIG_TOL)
        return cls(mean, Phi, sigma_sq, resid_tol=resid_tol)

    def update_from_samples(self, U, weights=None, eig_tol=_EIG_TOL):
        Phi, sigma_sq, mean, resid_tol = pca_from_samples(U, weights=weights, eig_tol=_EIG_TOL)
       
        self.mean = mean
        if self.Wm is not None:
            self.P_W_mean, self.P_W_mean_coords = self._Wm.project(self.mean, return_coeffs=True)
        self.Phi = Phi
        self.sigma_sq = sigma_sq
        self._resid_tol = resid_tol

    def variance(self):
        return np.sqrt(self.sigma_sq.prod())

    @property
    def Wm(self):
        return self._Wm
    @Wm.setter
    def Wm(self, new_Wm):
        if new_Wm is not self._Wm:
            if new_Wm is not None:
                if not new_Wm.is_orthonormal:
                    raise Exception( f'{self.__class__.__name__}: Wm must be orthonormal')

            if self._Wm is not None:
                warnings.warn(f'{self.__class__.__name__}: Setting new Wm space')

            self._Wm = new_Wm
            self._GW_is_stale = True
            if self._Wm is not None:
                self.P_W_mean, self.P_W_mean_coords = self._Wm.project(self.mean, return_coeffs=True)

    @property
    def sigma_sq(self):
        return self._sigma_sq
    @sigma_sq.setter
    def sigma_sq(self, new_sigma_sq):
        self._sigma = np.sqrt(new_sigma_sq)
        self._sigma_sq = new_sigma_sq
        self._GW_is_stale = True

    @property
    def sigma(self):
        return self._sigma
    @sigma.setter
    def sigma(self, new_sigma):
        self._sigma = new_sigma
        self._sigma_sq = new_sigma**2
        self._GW_is_stale = True

    @property
    def Sigma(self):
        return np.diag(self._sigma)
    @property
    def variance(self):
        return np.sqrt(self._sigma_sq.sum())
    @property
    def variance_sq(self):
        return self._sigma_sq.sum()
    @property
    def Phi(self):
        return self._Phi
    @Phi.setter
    def Phi(self, new_Phi):
        if new_Phi is not self._Phi:
            if new_Phi is not None:
                if not new_Phi.is_orthonormal:
                    raise Exception( f'{self.__class__.__name__}: Phi must be orthonormal')

            if self._Phi is not None:
                warnings.warn(f'{self.__class__.__name__}: Setting new Vn space')

            self._Phi = new_Phi
            self._GW_is_stale = True

    def truncate(self, idx):
        return type(self)(self.mean, self.Phi[idx], self.sigma_sq[idx], Wm=self.Wm)

    @property
    def GW(self):
        if self._GW is None or self._GW_is_stale:
            self._update_GW()
        return self._GW

    def _update_GW(self):
        if self.Wm is not None:
            self._GW = self.Wm.A @ (self.sigma * self.Phi)
            self.r = np.linalg.matrix_rank(self._GW)
            self._Psi_tilde, self._gamma, self._Phi_tilde = np.linalg.svd(self._GW)
            self._Phi_tilde = self._Phi_tilde.T
            self._GW_is_stale = False
            self._solver = None
        else:
            raise Exception( f'{self.__class__.__name__}: No measurement space Wm set!')

    @property
    def GW_solver(self):
        if self._solver is None or self._GW_is_stale:
            self._solver = np.linalg.pinv(self.GW)
        return self._solver
 
    def cond_variance(self):
        # Calculate the variance conditional on the observation, note that 
        # it doesn't depend on w.
        self._update_GW()
        Phi_w = self._Phi_tilde[:,:self.r]
        self._cond_variance = np.linalg.det((Phi_w.T/self.sigma_sq) @ Phi_w)

        return self._cond_variance
    
    def _w_mf_coords(self, w_coords):        
        w_mf_coords = (w_coords.T - self.P_W_mean_coords).T
        if self.Wm.n == 1 and w_mf_coords.shape[0] != 1:
            return np.atleast_2d(w_mf_coords)
        return w_mf_coords
    
    def residual_sq(self, u):
        u_mf = u - self.mean

        # How far away is our measurement from the reduced model?
        return u_mf.norms_sq() - np.linalg.norm(self.Phi.A @ u_mf, axis=0)**2

    def residual(self, u):
        return np.sqrt(self.residual_sq(u))
 
    def Wm_residual(self, w_coords):
        # How far away is our measurement from the Wm image of the reduced model?
        self._update_GW()
        return np.linalg.norm(self._Psi_tilde[:,self.r:].T @ self._w_mf_coords(w_coords), axis=0).T
    
    def in_span(self, u, resid_tol=None):
        if resid_tol is None:
            resid_tol = self._resid_tol
         
        return (self.residual_sq(u) <= self._resid_tol)
   
    def in_Wm_span(self, w_coords, resid_tol=None):
        self._update_GW()
        u_tilde_perp_coords = np.linalg.norm(self._Psi_tilde[:,self.r:].T @ self._w_mf_coords(w_coords), axis=0).T
        if resid_tol is None:
            resid_tol = self._resid_tol
        
        return (u_tilde_perp_coords <= resid_tol)
   
    def density(self, u):
        dists = np.linalg.norm(((self.Phi.A @ (u - self.mean)).T / self.sigma).T, axis=0).T

        return np.exp(-0.5 * dists**2) / (np.sqrt((2*np.pi))**self.Phi.n * self.sigma.prod())

    def log_density(self, u):
        dists = np.linalg.norm(((self.Phi.A @ (u - self.mean)).T / self.sigma).T, axis=0).T

        return - 0.5 * dists**2 - 0.5 * self.Phi.n * np.log(2*np.pi) - np.log(self.sigma).sum()

    def marginal(self, w_coords):
        # Calculate the normalising constant to give the conditional density perpindicular to Wm
        self._update_GW()
        u_tilde_coords = ((self._Psi_tilde[:,:self.r].T @ self._w_mf_coords(w_coords)).T / self._gamma[:self.r]).T

        exp_tilde = np.exp(-0.5 * u_tilde_coords**2)
        adj = np.sqrt(2*np.pi) * self._gamma[:self.r]
        
        return (exp_tilde.T / adj).T.prod(axis=0)

    def log_marginal(self, w_coords):
        # Calculate the normalising constant to give the conditional density perpindicular to Wm
        self._update_GW()
        
        u_tilde_coords = ((self._Psi_tilde[:,:self.r].T @ self._w_mf_coords(w_coords)).T / self._gamma[:self.r]).T
        return (-0.5 * u_tilde_coords.T**2 - np.log(self._gamma)).T.sum(axis=0) - 0.5 * self.r * np.log(2*np.pi)

    def best_estimate(self, w_coords):
        """ w is the coordinates relative to the Wm basis of the measurement to
            reconstruct. This routine returns a Vector if w is an np.ndarray of
            length Wm.n """
        return self.mean + (self.sigma * self.Phi) @ self.best_estimate_mf_coords(w_coords)

    def best_estimate_mf_coords(self, w_coords):
        # mean-free coordinates in the Phi basis
        self._update_GW()
        return self.GW_solver @ self._w_mf_coords(w_coords)

