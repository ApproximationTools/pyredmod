"""
greedy_methods.py

Author: James Ashton Nichols
Start date: December 2018

These classes are for greedy construction of Vn given a particular measured u in a particular basis Wm. 
One feature is the use of only the R^(n x m) representation of everything, saving the time of the inner
product calculations in V_h or V, the ambient solution space

TODO: The residual error of the greedy basis over the training set is used widely (e.g. in the "poor man's
estimate". It'd be worth re-writing this class reasonably well in a future reboot of the code base
"""

import math
import numpy as np
import warnings
import copy

import pyhilbert as hil

from pyredmod.pyredmod.lin_approx import *

# This constant determines linear dependence in terms of max difference of norm (roughly sqrt machine tol)
_linear_dependence_tol = 1e-8

class LinearlyDependent(Exception): pass

class GreedyApprox(object):

    def __init__(self, dictionary, Vn=None, verbose=False):
        """ We need to be either given a dictionary or a point generator that produces d-dimensional points
            from which we generate the dictionary. """
        
        self.dictionary = dictionary
        
        self.Vn = Vn # or hil.Basis()
        
        self.verbose = verbose
        self.sel_crit = np.array([])
        self.residuals = np.array([])
        self.dict_sel = np.array([], dtype=np.int32)

    @property
    def n(self):
        return self.Vn.n
    
    def initial_choice(self):
        """ Different greedy methods will have their own maximising/minimising criteria, so all 
        inheritors of this class are expected to overwrite this method to suit their needs. """
       
        self.norms = self.dictionary.norms()

        n0 = np.argmax(self.norms)
        crit = resid = self.norms[n0]

        if self.Vn is None:
            self.Vn = hil.Basis(self.dictionary[n0].values, space=self.dictionary[n0].space)
        else:
            self.Vn.append(self.dictionary[n0])

        return n0, crit, resid
 

    def next_step_choice(self):
        """ Different greedy methods will have their own maximising/minimising criteria, so all 
        inheritors of this class are expected to overwrite this method to suit their needs. """
    
        # We go through the dictionary and find the max of residuals normalised
        residuals = self.dictionary - self.Vn.project(self.dictionary)
        residual_norms = residuals.norms()
        
        if np.all(np.isclose(residual_norms, 0.0, atol=_linear_dependence_tol)):
            raise LinearlyDependent()
         
        ni = np.argmax(residual_norms)
        crit = resid = residual_norms[ni]
        self.Vn.append(self.dictionary[ni])
        
        return ni, crit, resid
    
    def construct_to_n(self, n_goal):
         
        if self.verbose:
            print('i \t Selection \t Sel. criteria')
        
        if self.Vn is None or self.Vn.n == 0:
            n0, crit, resid = self.initial_choice()

            self.dict_sel = np.append(self.dict_sel, n0)
            self.sel_crit = np.append(self.sel_crit, crit)
            self.residuals = np.append(self.residuals, resid)

            if self.verbose:
                print('{0} : \t {1} \t {2}'.format(self.Vn.n, n0, crit))

        try: 
            while self.Vn.n < n_goal:
                
                ni, crit, resid = self.next_step_choice()
                
                self.dict_sel = np.append(self.dict_sel, ni) 
                self.sel_crit = np.append(self.sel_crit, crit)
                self.residuals = np.append(self.residuals, resid)

                if self.verbose:
                    print('{0} : \t {1} \t\t {2}'.format(self.Vn.n, ni, crit))

        except LinearlyDependent:
            print('Vn spans all dictionary points at n={0}, stopping greedy'.format(self.n))

        if self.verbose:
            print('Done!')
        
        return self.Vn

    def construct_to_poor_mans_criteria(self, Wm):
        if self.verbose:
            print('Greedy construction to "poor man\'s" criteria')

        
        self.construct_to_n(Wm.n+1)
        self.betas = np.zeros(self.Vn.n)

        # Note that span(Vn[:i]) = span(Vn.orthonormalise()[:i]) as a cholesky
        # upper triangular matrix is used in the orthonormalisation, hence we
        # can take this short-cut step here:
        CG = Wm.A @ self.Vn.orthonormalise()[:-1]
        for i in range(self.Vn.n):

            gamma = scipy.linalg.svd(np.atleast_2d(CG[:,:i+1]), compute_uv=False)
            self.betas[i] = gamma[-1]

        poor_mans_crit = self.residuals[1:] / self.betas[:-1]
        self.poor_mans_dim = np.argmin(poor_mans_crit) + 1
        if self.verbose:
            print(f'minimum of residual * stability at dimension {self.poor_mans_dim}: {np.min(poor_mans_crit)}')
        
        return self.Vn[:self.poor_mans_dim], self.residuals[self.poor_mans_dim], self.betas[self.poor_mans_dim - 1]

class MeasBasedOMP(GreedyApprox):
    """ Measurement based greedy orthogonal matching pursuit """ 

    def __init__(self, dictionary, u, Wm, Vn=None, verbose=False):

        super().__init__(dictionary, Vn=Vn, verbose=verbose)

        if not Wm.is_orthonormal:
            raise Exception('Need orthonormal Wm for greedy approx construction')

        self.Wm = Wm
        self.w, self.w_coeffs = self.Wm.project(u, return_coeffs=True)

        self.LinApprox = None
        self.beta = np.zeros(self.m)

        self.Wdict = self.Wm.A @ self.dictionary
        self.Wdict /= np.linalg.norm(self.Wdict, axis=0)
        
        self.Zn = None

    @property
    def m(self):
        return self.Wm.n
 
    def reset_u(self, u):
        # This is to reset with a new u but same dictionary dots, save lots of time...
        self.w, self.w_coeffs = self.Wm.project(u, return_coeffs=True)
        self.Vn = None 
        self.LinApprox = None
        self.beta = np.zeros(self.m)

        self.sel_crit = np.array([])
        self.dict_sel = np.array([], dtype=np.int32)

    def construct_to_n(self, n_goal):

        self.beta.resize(n_goal)
        super().construct_to_n(n_goal)

        return self.Vn

    def initial_choice(self):

        p_V_d = np.abs(self.Wdict.T @ self.w_coeffs)
        n0 = np.argmax(p_V_d)
        crit = p_V_d[n0]

        if self.Vn is None:
            self.Vn = hil.Basis(self.dictionary[n0].values, space=self.dictionary[n0].space)
        else:
            self.Vn.append(self.dictionary[n0])

        self.Zn = self.Wdict[:, n0][:,np.newaxis]

        if self.LinApprox is None or self.LinApprox.Vn is not self.Vn.orthonormal_basis:
            self.LinApprox = LinearWorstCaseEstimator(Vn = self.Vn.orthonormalise(), Wm = self.Wm)
        self.beta[self.n - 1] = self.LinApprox.beta()
    
        return n0, crit

    def next_step_choice(self):
        """ Different greedy methods will have their own maximising/minimising criteria, so all 
        inheritors of this class are expected to overwrite this method to suit their needs. """
        
        w_perp =  self.w_coeffs - np.linalg.lstsq(self.Zn, self.w_coeffs, rcond=None)[0] @ self.Zn.T
        
        #p_V_d = np.abs(np.dot(w_perp, self.Wdict.T)) #self.Wm.dot(v_perp)))
        p_V_d = np.abs(self.Wdict.T @ w_perp)

        if np.all(np.isclose(p_V_d, 0.0, atol=_linear_dependence_tol)):
            raise LinearlyDependent()

        ni = np.argmax(p_V_d)
        crit = p_V_d[ni]

        self.Vn.append(self.dictionary[ni])
        #self.Zn = np.hstack((self.Zn, self.Wdict[ni, :][:,np.newaxis]))
        self.Zn = np.hstack((self.Zn, self.Wdict[:, ni][:,np.newaxis]))
        
        self.LinApprox.append_Vn(self.dictionary[ni])
        self.beta[self.n-1] = self.LinApprox.beta()

        return ni, crit

class MeasBasedPP(GreedyApprox):
    """ Measurement based greedy orthogonal matching pursuit """ 

    def __init__(self, dictionary, u, Wm, Vn=None, verbose=False):
 
        if not Wm.is_orthonormal:
            raise Exception('Need orthonormal Wm for greedy approx construction')

        self.Wm = Wm
        self.w, self.w_coeffs = self.Wm.project(u, return_coeffs=True) 

        self.LinApprox = None
        self.beta = np.zeros(self.m)

        super().__init__(dictionary, Vn=Vn, verbose=verbose)

        #self.Wdict = dictionary.A @ self.Wm
        #self.Wdict /= np.linalg.norm(self.Wdict, axis=1)

        self.Wdict = self.Wm.A @ dictionary
        self.Wdict /= np.linalg.norm(self.Wdict, axis=0)

        #self.Wdict = np.zeros((len(dictionary), self.m))
        #for i, v in enumerate(dictionary):
        #    self.Wdict[i, :] = self.Wm.dot(v)
        #    self.Wdict[i, :] /= np.linalg.norm(self.Wdict[i,:]) # NOTE - Should normalise here

        self.Zn = None

    @property
    def m(self):
        return self.Wm.n
 
    def reset_u(self, u):
        # This is to reset with a new u but same dictionary dots, save lots of time...
        self.w, self.w_coeffs = self.Wm.project(u, return_coeffs=True)
        self.Vn = None
        self.LinApprox = None
        self.beta = np.zeros(self.m)

        self.sel_crit = np.array([])
        self.dict_sel = np.array([], dtype=np.int32)

    def construct_to_n(self, n_goal):

        self.beta.resize(n_goal)
        super().construct_to_n(n_goal)

        return self.Vn

    def initial_choice(self):

        #p_V_d = np.abs(np.dot(self.w_coeffs, self.Wdict.T))
        p_V_d = np.abs(self.Wdict.T @ self.w_coeffs)
        n0 = np.argmax(p_V_d)
        crit = p_V_d[n0]

        if self.Vn is None:
            self.Vn = hil.Basis(self.dictionary[n0].values, space=self.dictionary[n0].space)
        else:
            self.Vn.append(self.dictionary[n0])
       
        #self.Zn = self.Wdict[n0,:][:,np.newaxis]
        self.Zn = self.Wdict[:, n0][:,np.newaxis]

        if self.LinApprox is None or self.LinApprox.Vn is not self.Vn.orthonormal_basis:
            self.LinApprox = LinearWorstCaseEstimator(Vn = self.Vn.orthonormalise(), Wm = self.Wm)
        self.beta[self.n - 1] = self.LinApprox.beta()
    
        return n0, crit

    def next_step_choice(self):
        """ Different greedy methods will have their own maximising/minimising criteria, so all 
        inheritors of this class are expected to overwrite this method to suit their needs. """
        
        p_V_d = np.zeros(len(self.dictionary))
        
        for j, z in enumerate(self.Wdict.T):
            Zn_ext =  np.hstack((self.Zn, z[:,np.newaxis]))
            w_perp = self.w_coeffs - np.linalg.lstsq(Zn_ext, self.w_coeffs, rcond=10*max(Zn_ext.shape)*np.finfo(np.float64).eps)[0] @ Zn_ext.T
            p_V_d[j] = np.linalg.norm(w_perp)
       
        if np.any(np.isclose(p_V_d, 0.0, atol=_linear_dependence_tol)):
            raise LinearlyDependent()

        ni = np.argmin(p_V_d)
        crit = p_V_d[ni]

        self.Vn.append(self.dictionary[ni])
        #self.Zn = np.hstack((self.Zn, self.Wdict[ni, :][:,np.newaxis]))
        self.Zn = np.hstack((self.Zn, self.Wdict[:,ni][:,np.newaxis]))
        
        self.LinApprox.append_Vn(self.dictionary[ni])
        self.beta[self.n-1] = self.LinApprox.beta()

        return ni, crit
