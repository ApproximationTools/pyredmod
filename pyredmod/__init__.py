"""

pyRedMod : Reduced Modelling for Python. 
==============================================================================

Contents
--------
pyredmod classes for linear and nonlinear approximation


"""

from .lin_approx import *
from .gauss_approx import *
from .pw_lin_approx import *
from .tree_pw_lin_approx import *
from .mixture_approx import *
from .tree_decomp_mixture import *
from .tree_decomp_wc import *
from .utils import *
from .greedy_methods import *
